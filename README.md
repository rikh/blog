# blog

Source code for my blog at <https://huijzer.xyz>.
To see how to build everything locally, see `.gitlab-ci.yml`.

## Licenses

### Core:

- The source code for the blog is MIT licensed.
- The blog itself uses the [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/) license.

### External:

- Franklin, FranlinTemplates and LiveServer are all MIT licensed.
- abhishalya is [MIT licensed](https://github.com/abhishalya/abhishalya.github.io/).
- The SVG vector for the copy button is created by Vaneet Thakur and has the CC Attribution License.
