+++
title = "About"
rss = "About this website"
image = "/assets/about/output/basic-plot.svg"
+++

\toc

#### Publications

- Storopoli, Huijzer and Alonso (2021). Julia Data Science. \
    ISBN: 9798489859165
    ([html](https://juliadatascience.io),
    [pdf](https://juliadatascience.io/juliadatascience.pdf))

- Huijzer, Blaauw, and den Hartigh (2023).
    SIRUS.jl: Interpretable Machine Learning via Rule Extraction.
    Journal of Open Source Software, 8(90), 5786,
    ([html](https://joss.theoj.org/papers/10.21105/joss.05786),
    [pdf](https://www.theoj.org/joss-papers/joss.05786/10.21105.joss.05786.pdf))

- Huijzer, de Jonge, Blaauw, Baatenburg de Jong, de Wit, den Hartigh (2023).
    Predicting Special Forces Dropout via Explainable Machine Learning
    ([html](https://doi.org/10.31234/osf.io/s6j3r),
    [pdf](https://psyarxiv.com/s6j3r/download?format=pdf))

#### Open Source

_Projects:_

- [PlutoStaticHTML.jl](https://github.com/rikhuijzer/PlutoStaticHTML.jl)
- [PowerAnalyses.org](https://poweranalyses.org)
- [Books.jl](https://github.com/JuliaBooks/books.jl)
- [TuringGLM.jl](https://github.com/TuringLang/TuringGLM.jl)
- [TuringModels.jl](https://github.com/StatisticalRethinkingJulia/TuringModels.jl)
- [SIRUS.jl](https://github.com/rikhuijzer/SIRUS.jl)

_Contributions:_

- [mlir][vector] Fix invalid `LoadOp` indices being created ([llvm/llvm-project#76292](https://github.com/llvm/llvm-project/pull/76292))
- [mlir][python][nfc] Test `-print-ir-after-all` ([llvm/llvm-project#75742](https://github.com/llvm/llvm-project/pull/75742))
- [mlir][vector] Fix crash on invalid `permutation_map` ([llvm/llvm-project#74925](https://github.com/llvm/llvm-project/pull/74925))
- [mlir][llvm] Fix negative GEP crash in type consistency ([llvm/llvm-project#74859](https://github.com/llvm/llvm-project/pull/74859))
- [mlir][llvm] Fix verifier for const int and dense ([llvm/llvm-project#74340](https://github.com/llvm/llvm-project/pull/74340))
- [mlir][llvm] Fix verifier for const float ([llvm/llvm-project#74247](https://github.com/llvm/llvm-project/pull/74247))
- [mlir][memref] Fix an invalid dim loop motion crash ([llvm/llvm-project#74204](https://github.com/llvm/llvm-project/pull/74204))
- [mlir] Fix a zero stride canonicalizer crash ([llvm/llvm-project#74200](https://github.com/llvm/llvm-project/pull/74200))
- [mlir][spirv][doc] Remove duplicate syntax formats ([llvm/llvm-project#73386](https://github.com/llvm/llvm-project/pull/73386))
- [mlir][vector] Fix a `target-rank=0` unrolling ([llvm/llvm-project#73365](https://github.com/llvm/llvm-project/pull/73365))
- [mlir][affine] Fix dim index out of bounds crash ([llvm/llvm-project#73266](https://github.com/llvm/llvm-project/pull/73266))
- [mlir][async] Avoid crash when not using `func.func` ([llvm/llvm-project#72801](https://github.com/llvm/llvm-project/pull/72801))
- [mlir][tensor] Fold when source is const ([llvm/llvm-project#71643](https://github.com/llvm/llvm-project/pull/71643))
- [mlir] Verify TestBuiltinAttributeInterfaces eltype ([llvm/llvm-project#69878](https://github.com/llvm/llvm-project/pull/69878))
- [mlir][arith] Fix canon pattern for large ints in chained arith ([llvm/llvm-project#68900](https://github.com/llvm/llvm-project/pull/68900))
- [mlir][test] Avoid buffer overflow for `test.verifiers` ([llvm/llvm-project#D154792](https://github.com/llvm/llvm-project/commit/493fbc4c8e3f4303e5660e335f169c55462c9618))
- [mlir][tosa] Pass encoding through `tosa-to-linalg` ([llvm/llvm-project#D152171](https://github.com/llvm/llvm-project/commit/c8ac14d754088b19c659ca0915229f1f28776831))
- [mlir][affine] Improve load elimination ([llvm/llvm-project#D154769](https://github.com/llvm/llvm-project/commit/71513a71cdf380efd6a44be6939e2cb979a62407))
- [mlir][tosa] Fix fp canonicalization for `clamp` ([llvm/llvm-project#D152123](https://github.com/llvm/llvm-project/commit/cfdea8f5bb6b0138719c5b4f3fe1616b6ea915a7))
- [mlir] Avoid folding `index.remu` and `index.rems` for 0 rhs ([llvm/llvm-project#D151476](https://github.com/llvm/llvm-project/commit/2e4e218474320abf480c39d3b968a5a09477ad03))
- Update `main.yml` workflow ([llvm/mlir-www#152](https://github.com/llvm/mlir-www/pull/152))
- Improve macro hygiene ([JuliaLang/PrecompileTools.jl#9](https://github.com/JuliaLang/PrecompileTools.jl/pull/9))
- Fix wrong formula order ([JuliaAI/MLJGLMInterface.jl#35](https://github.com/JuliaAI/MLJGLMInterface.jl/pull/35))
- microsoft-edge: fix file picker and `substituteInPlace` ([NixOS/nixpkgs#219170](https://github.com/NixOS/nixpkgs/pull/219170))
- Use `SnoopPrecompile` ([fonsp/Pluto.jl#2441](https://github.com/fonsp/Pluto.jl/pull/2441))
- Document parameters for noncentral distributions ([JuliaStats/Distributions.jl#1632](https://github.com/JuliaStats/Distributions.jl/pull/1632))
- Build and run `MyLib` ([JuliaLang/PackageCompiler.jl#732](https://github.com/JuliaLang/PackageCompiler.jl/pull/732))
- Fix undefined `SnoopPrecompile` ([timholy/SnoopCompile.jl#287](https://github.com/timholy/SnoopCompile.jl/pull/287))
- Fix `print_tree` ([JuliaAI/DecisionTree.jl#185](https://github.com/JuliaAI/DecisionTree.jl/pull/185))
- Test multiple seeds ([JuliaAI/DecisionTree.jl#174](https://github.com/JuliaAI/DecisionTree.jl/pull/174))
- Replace `L` by `<` and `R` by `≥` ([JuliaAI/DecisionTree.jl#172](https://github.com/JuliaAI/DecisionTree.jl/pull/172))
- Use `uint16` ([JuliaWeb/HTTP.jl#845](https://github.com/JuliaWeb/HTTP.jl/pull/845))
- Also trigger compilation for Unicode ([fonsp/Pluto.jl#2161](https://github.com/fonsp/Pluto.jl/pull/2161))
- Extend docs for `@max_methods` ([JuliaLang/julia#45595](https://github.com/JuliaLang/julia/pull/45595))
- Refactor `ExpressionExplorer.explore!` ([fonsp/Pluto.jl#2155](https://github.com/fonsp/Pluto.jl/pull/2155))
- Document serialization and reduce duplication in `PlutoRunner` ([fonsp/Pluto.jl#2145](https://github.com/fonsp/Pluto.jl/pull/2145))
- Clarify error for uninstantiated model further ([JuliaAI/MLJTuning.jl#178](https://github.com/JuliaAI/MLJTuning.jl/pull/178))
- Set `permissions: write-all` in `hosting.md` ([JuliaDocs/Documenter.jl#1819](https://github.com/JuliaDocs/Documenter.jl/pull/1819))
- Add std to show for `PerformanceEvaluation` ([JuliaAI/MLJBase.jl#766](https://github.com/JuliaAI/MLJBase.jl/pull/766))
- Reduce overspecializations in PlutoRunner.format_output ([fonsp/Pluto.jl#2062](https://github.com/fonsp/Pluto.jl/pull/2062))
- Fix wrong field name in a `show` method ([JuliaAI/MLJBase.jl#758](https://github.com/JuliaAI/MLJBase.jl/pull/758))
- Reduce time to first `from_flat_kwargs` ([fonsp/Pluto.jl#2005](https://github.com/fonsp/Pluto.jl/pull/2005))
- Optimize `maybe_macroexpand` ([fonsp/Pluto.jl#1991](https://github.com/fonsp/Pluto.jl/pull/1991))
- Fix tests on Julia 1.8-beta1 ([JuliaArrays/LazyArrays.jl#205](https://github.com/JuliaArrays/LazyArrays.jl/pull/205))
- Fix ordering of nested elements when `sortby = :firstexec` ([KristofferC/TimerOutputs.jl#144](https://github.com/KristofferC/TimerOutputs.jl/pull/144))
- Show timing and allocations for tests ([TuringLang/Turing.jl#1787](https://github.com/TuringLang/Turing.jl/pull/1787))
- Add a benchmark for compilation times ([fonsp/Pluto.jl#1959](https://github.com/fonsp/Pluto.jl/pull/1959))
- Use `precompile` for `SessionActions.open` ([fonsp/Pluto.jl#1934](https://github.com/fonsp/Pluto.jl/pull/1934))
- Add `TapedTask` type annotations to `storage[:tapedtask]` ([TuringLang/Libtask.jl#121](https://github.com/TuringLang/Libtask.jl/pull/121))
- Replace `eval` by generated function ([TuringLang/Libtask.jl#119](https://github.com/TuringLang/Libtask.jl/pull/119))
- Fix non-concrete type near TRCache ([TuringLang/Libtask.jl#118](https://github.com/TuringLang/Libtask.jl/pull/118))
- Extend cache action ([julia-actions/cache#4](https://github.com/julia-actions/cache/pull/4))
- Make symbols in deprecation warnings more explicit ([Distributions.jl#1428](https://github.com/JuliaStats/Distributions.jl/pull/1428))
- Add R-square R² (coefficient of determination) ([MLJBase.jl#679](https://github.com/JuliaAI/MLJBase.jl/pull/679))
- Report feature names in `fitresult` ([MLJGLMInterface.jl#12](https://github.com/JuliaAI/MLJGLMInterface.jl/pull/12))
- Add deploy check ([JuliaLang/www.julialang.org#1364](https://github.com/JuliaLang/www.julialang.org/pull/1364))
- Allow Ints ([EffectSizes.jl#24](https://github.com/harryscholes/EffectSizes.jl/pull/24))
- Document the `Explicit` tuning strategy ([MLJ.jl#836](https://github.com/alan-turing-institute/MLJ.jl/pull/836))
- Add where syntax to rules ([SymbolicUtils.jl#329](https://github.com/JuliaSymbolics/SymbolicUtils.jl/pull/329))
- Deprecate old package name ([General#41692](https://github.com/JuliaRegistries/General/pull/41692))
- Setup automated builds for TuringTutorials ([TuringTutorials#123](https://github.com/TuringLang/TuringTutorials/pull/123))
- Add t-test for cases where only mean and sd are known ([HypothesisTests.jl#237](https://github.com/JuliaStats/HypothesisTests.jl/pull/237))
- Update documentation for density ([AlgebraOfGraphics.jl#211](https://github.com/JuliaPlots/AlgebraOfGraphics.jl/pull/211))
- Add tests for authentication ([Pluto.jl#1170](https://github.com/fonsp/Pluto.jl/pull/1170))
- Fix InExactError for `[0.0]` ([Showoff.jl#41](https://github.com/JuliaGraphics/Showoff.jl/pull/41))
- Improve type stability for tryparse VersionNumber ([julia#40557](https://github.com/JuliaLang/julia/pull/40557))
- Add quantile bars statistic ([Gadfly.jl#1521](https://github.com/GiovineItalia/Gadfly.jl/pull/1521))
- Add Gadfly examples ([MCMCChains.jl#275](https://github.com/TuringLang/MCMCChains.jl/pull/275))
- Add Documenter.jl ([MCMCChains.jl#265](https://github.com/TuringLang/MCMCChains.jl/pull/265))
- Refactor ([Turing.jl#1548](https://github.com/TuringLang/Turing.jl/pull/1548))
- Refactor ([StasticalRethinking.jl#104](https://github.com/StatisticalRethinkingJulia/StatisticalRethinking.jl/pull/104))
- Refactor ([Plots.jl#3200](https://github.com/JuliaPlots/Plots.jl/pull/3200))
- Switch to GitHub Actions ([Compose.jl#408](https://github.com/GiovineItalia/Compose.jl/pull/408))
- Refactor ([YAML.jl#103](https://github.com/JuliaData/YAML.jl/pull/10))
- Bugfix ([Pkg.jl#2297](https://github.com/JuliaLang/Pkg.jl/pull/2297))
- Add CompatHelper ([Pluto.jl#766](https://github.com/fonsp/Pluto.jl/pull/766))
- Document Response and Request constructors ([HTTP.jl#644](https://github.com/JuliaWeb/HTTP.jl/pull/644))
- Print stack trace ([Franklin.jl#718](https://github.com/tlienart/Franklin.jl/pull/718))
- Reduce number of CI jobs ([PkgTemplates.jl#252](https://github.com/invenia/PkgTemplates.jl/pull/252))
- Add clear_cache ([Memoize.jl#60](https://github.com/JuliaCollections/Memoize.jl/pull/60))
- Add flex to Jemdoc ([FranklinTemplates.jl#99](https://github.com/tlienart/FranklinTemplates.jl/pull/99))
- Change to GitHub CI ([Gadfly.jl#1495](https://github.com/GiovineItalia/Gadfly.jl/pull/1495))
- Update MbedTLS version ([OAuth.jl#29](https://github.com/randyzwitch/OAuth.jl/pull/29))

#### Supervision

_MSc thesis co-advisor:_

- Tom Jalink, 2022. _Een voorspellend model voor uitval in de vooropleiding en de elementaire commando opleiding_
- Laura Haasper, 2022. _Personality traits in the Dutch Special Forces_
- Daniel Buxton, 2021. _Het proces van stress en herstel tijdens het selectietraject van het Korps Commandotroepen_
- Calum Guthrie, 2020. _What are the Characteristics of Successful Commando Candidates?_

#### Presentations

- _Strengths AND weaknesses of Julia compared to Python_ at [Pygrunn](https://pygrunn.org/)
  (Nov 2021)
- _Bayesian statistics with Julia: visualizing uncertainty_ (Oct 2020)
- _Deep learning and natural language processing_ at [PyGrunn](https://pygrunn.org/) (Jun 2019)

#### Education

~~~
  <b>Eindhoven University of Technology</b><br>
  <b>MSc</b>, Computer Science and Engineering, Software Science, Mar 2019.<br>
  &nbsp;&nbsp;&nbsp;&nbsp;Thesis: <a href="https://research.tue.nl/en/studentTheses/automatically-responding-to-customers">Automatically responding to customers</a>.<br>
  &nbsp;&nbsp;&nbsp;&nbsp;Committee: dr. Nikolay <a href="http://www.yakovets.ca">Yakovets</a>, dr. George Fletcher, and dr. Joaquin Vanschoren.&nbsp;&nbsp;<br>

  <br>
  <b>Eindhoven University of Technology</b><br>
  Premaster Computer Science and Engineering, Software Science, Feb 2017.<br>
  <br>
  <b>Hanze University of Applied Sciences</b><br>
  <b>Bsc</b>, Mechanical Engineering, Feb 2015.
~~~
