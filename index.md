+++
title = "Home"
rss = "Blog posts about statistics, research and related topics"
image = "/assets/self.jpg"
+++

This is my personal website where I post about technology-related topics.

Contact: [email](mailto: huijzer.xyzcontact.stylishly377@passmail.net)

## Posts

{{blogposts}}
