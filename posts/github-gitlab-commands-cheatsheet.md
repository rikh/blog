+++
title = "GitHub and GitLab commands cheatsheet"
published = "2020-12-16"
tags = ["shortcut", "git"]
rss = "A cheatsheet for commands like 'Fix #2334'."
image = "/assets/self.jpg"
+++

Both GitHub and GitLab provide shortcuts for interacting with the layers they have built on top of Git.
These shortcuts are a convenient and clean way to interact with things like issues and PRs.
For instance, using `Fixes #2334` in a commit message will close issue #2334 automatically when the commit is applied to the main branch.
However, the layers on top of Git differ between the two, and therefore the commands will differ as well.
This document is a cheatsheet for issue closing commands; I plan to add more of these commands over time.

*If this page contains outdated information, suggest a fix via [GitHub](https://github.com/rikhuijzer/huijzer.xyz).*

\toc

## Get your branches again after a shallow clone

For large repositories, it is often useful (or necessary even) to make a shallow clone.
For example, when cloning your fork at `someusername` of the Rust language, you can use:

```
git clone --depth=1 https://github.com/someusername/rust
```

However, now all branches are missing so you cannot continue working on your PRs.
To fix this, use:

```
git remote set-branches origin '*'

git fetch -v --depth=1

git checkout the-pr-branch
```

Thanks to `marlo` and `jthill` on [StackOverflow](https://stackoverflow.com/a/27393574/5056635).

As a side note, sometimes it's useful to merge upstream into the PR branch from the GitHub web interface.
To do this, browse to the branch inside the fork and click on "Sync fork".
This will merge the upstream's `main` into the branch.

## Stashing changes

For some reason, I've never been comfortable with temporarily stashing my changes away via Git stash.
However, a very simple alternative is to just write the diff to a file via

```
git diff > mychanges.patch
```

At a later point, this file can be used to restore the changes via

```
git apply mychanges.patch
```

## Delete one token from the Git credential manager

To delete only one token, use

```sh
git credential-store erase
```

For example, to erase the token for `https://github.com`, use

```sh
echo -e "protocol=https\nhost=github.com" | git credential-store erase
```

## Close an issue via a pull request title or commit message

### GitHub
On GitHub use
([docs](https://docs.github.com/en/free-pro-team@latest/github/managing-your-work-on-github/linking-a-pull-request-to-an-issue#linking-a-pull-request-to-an-issue-using-a-keyword)):

- close
- closes
- closed
- fix
- fixes
- fixed
- resolve
- resolves
- resolved

**Examples**

```c
Fix #2334
Fix octo-org/octo-repo#2334
Fix #2334, fix octo-org/octo-repo#2334
```

\
### GitLab
On GitLab use
([docs](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues)):

- Close, Closes, Closed, Closing, close, closes, closed, closing 
- Fix, Fixes, Fixed, Fixing, fix, fixes, fixed, fixing 
- Resolve, Resolves, Resolved, Resolving, resolve, resolves, resolved, resolving 
- Implement, Implements, Implemented, Implementing, implement, implements, implemented, implementing 

**Examples**

```c
Fixes #2334
Closes #2334
Closes #4, #6
Closes group/project#123
```
