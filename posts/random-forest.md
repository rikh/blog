+++
title = "Random forest classification in Julia"
published = "2021-01-21"
tags = ["simulating data", "machine learning"]
rss = "Fitting a random forest classifier and reporting accuracy metrics."
image = "/assets/self.jpg"
reeval = true
+++

\readhtml{random-forest}
